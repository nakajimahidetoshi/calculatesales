package jp.alhinc.nakajima_hidetoshi.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales{

	public static void main(String[]args) {
		Map<String,String>branchMap=new HashMap<String,String>();
		Map<String,Long>salesMap=new HashMap<String,Long>();
		//コマンドライン引数が1つじゃない場合
		if (args.length!=1){
		    System.out.println("予期せぬエラーが発生しました");
		    return;
		}
		if(!branchName(args[0],"branch.out",branchMap,salesMap)){
			return;
		}
		//ディレクトリ内のファイルを読み込む
		//売上ファイルのリストを作成
		File dir = new File(args[0]);
        File[] list = dir.listFiles();
        List<String> fileNameList=new ArrayList<String>();
        //売上ファイルがファイルではないときのエラー
        //半角数字8桁のrcdファイルのみ表示
        for(int i=0; i<list.length; i++) {
        	if(list[i].getName().matches("^[0-9]{8}.rcd$")&&list[i].isFile()) {
        		//File型をString型に変換
        		fileNameList.add(list[i].getName());
        	}
        }

        //rcdのリストの数だけループ
        for(int j=0;j<fileNameList.size()-1;j++) {
    		int rcd1 = Integer.parseInt(fileNameList.get(j).substring(0, 8));
    		int rcd2 = Integer.parseInt(fileNameList.get(j+1).substring(0, 8));
			//売上ファイルが連番になっているか
			if(rcd2-rcd1!=1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
        }
		for(int j=0;j<fileNameList.size();j++) {
			BufferedReader br1=null;
			try {
				File branchFile=new File(args[0],fileNameList.get(j));
				FileReader bfr=new FileReader(branchFile);
				br1=new BufferedReader(bfr);
				//売上ファイルのリスト化
				String branch;
				List<String> fileContents = new ArrayList<String>();

				while((branch=br1.readLine())!=null) {
					fileContents.add(branch);

				}
				//存在しない支店コードのエラー
				if(!branchMap.containsKey(fileContents.get(0))) {
					System.out.println(fileNameList.get(j)+"の支店コードが不正です");
					return;
				}
				//売上ファイルの行数を読む
				if(fileContents.size()!=2){
					System.out.println(fileNameList.get(j)+"のフォーマットが不正です");
					return;
				}
				//売上金額のエラー
				if(!fileContents.get(1).matches("\\d{1,10}")) {
	        		System.out.println("予期せぬエラーが発生しました");
	        		return;
				}
				fileContents.get(1);
				//String型をLong型に変更
				long salesAmount = Long.parseLong(fileContents.get(1));
				salesAmount = salesAmount + salesMap.get(fileContents.get(0));
				salesMap.put(fileContents.get(0),salesAmount);
				//合計金額のエラー
				if(salesAmount>9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
	    	}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if(br1!=null) {
					try {
						br1.close();
					}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
        }
		if (!branchOut(args[0],"branch.out",branchMap,salesMap)) {
			return;
		}
	}
	public static boolean branchName(String directoryPass, String branchOut, Map<String, String> branchMap, Map<String, Long> salesMap) {
		BufferedReader br=null;
		try {
			//支店定義ファイルを開く
			File file=new File(directoryPass,"branch.lst");
			//支店定義ファイルが存在しない場合のエラー
			if (file.exists()) {
				FileReader fr=new FileReader(file);
				br=new BufferedReader(fr);
			} else {
				System.out.println("支店定義ファイルが存在しません");
				return false;
	        }

			//支店定義ファイルを読み込み、支店コードと支店名に分ける
			String line;
			while((line=br.readLine())!=null) {
				String str = line;
				String[] name = str.split(",");

				if(name.length!=2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				//支店定義ファイルのマップを作る
				branchMap.put(name[0],name[1]);
				salesMap.put(name[0],0L);
				if(!name[0].matches("[0-9]{3}$")) {
	        		System.out.println("支店定義ファイルのフォーマットが不正です");
	        		return false;
				}
			}
		//支店別集計ファイルがロックされている場合のエラー
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(br!=null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
	 public static boolean branchOut(String directoryPass, String branchOut, Map<String, String> branchMap, Map<String, Long> salesMap) {
		//支店別集計ファイルの作成・出力
		BufferedWriter bw=null;
		String br2 = System.getProperty("line.separator");

		try {
			File branchOutFile=new File(directoryPass,branchOut);
			FileWriter fw = new FileWriter(branchOutFile);
			bw=new BufferedWriter(fw);
			for (String branchKey : branchMap.keySet()) {
			bw.write(branchKey+","+branchMap.get(branchKey)+","+salesMap.get(branchKey)+br2);
			}

		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw!=null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
